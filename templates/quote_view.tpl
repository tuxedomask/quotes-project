% setdefault('title','APPerture')
% include('templates/header.tpl', title=get('title'))
% setdefault('deletable', False)
<form action="" method="post" id="quoteDisplay">
	% for quote in quotes:
	<em>{{quote['quote']}}</em>
	<br />
	quote likes: {{quote['likes']}}
	<br />
	quote dislikes:{{quote['dislikes']}}
	<br /> 
	<input onclick="submitWithAction('/like', '{{quote['objectId']}}');" type="button" value="Like!">
	<input onclick="submitWithAction('/dislike', '{{quote['objectId']}}');" type="button" value="Dislike!">
		% if get('deletable'):
			<input onclick="submitWithAction('/delete_form', '{{quote['objectId']}}');" type="button" value="Delete" />
		% end
	<br /><br />
	% end
	<input id="qID" name="qID" type="hidden" value="" />
</form>
<script>
	function submitWithAction(action, id) {
		form = document.getElementById("quoteDisplay");
		form.action = action;
		document.getElementById('qID').value = id
		form.submit();
	}
</script>
% include('templates/footer.tpl')